import argparse
import hashlib

from config import SALT

parser = argparse.ArgumentParser(description='Generate hash from raw password;')

parser.add_argument('raw_password', type=str, help='raw password to be encrypted')

def _hash_password(password):
    return hashlib.sha512((password+SALT).encode('utf-8')).hexdigest()

if __name__ == '__main__':
    args = parser.parse_args()
    print(">>> YOUR PASSWORD HASH TO BE STORE IN DB <<<")
    print(_hash_password(args.raw_password))
    print(">>> END <<<")