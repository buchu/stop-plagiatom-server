import os
from tornado.options import define

define('PORT', 8888)
define('SECRET', "299c4e06a201684ef917fe7e8e16292e2acdc2c1")
define('SALT', 'EuX6XsQxd09opijSOGnKdw1/1WYCtR400ornqjRAgKgFuo0ewDl9JBEP45dlPF+7oPBs7Tau68iXjJUumekJSw==')

define('db_name', 'plagiator_db')
define('db_user', 'postgres')
define('db_pass', 'postgres')
define('db_host', os.getenv('DB_HOST'))
define('db_port', '5432')
