from models.base import BaseModel
from models.link import Link


class Report(BaseModel):
    id = None
    user_id = None
    status = None
    project_id = None
    links = []

    def __init__(self, id, user_id, status, project_id, links=None):
        self.id = id
        self.user_id = user_id
        self.status = status
        self.project_id = project_id
        self.links = links or []

    @classmethod
    async def get(cls, db, user_id, report_id):
        result = (await db.execute(
            "SELECT * FROM raports WHERE user_id = '{}' AND id = '{}'".format(user_id, report_id)
        )).fetchone()

        report = cls(*result) if result else None

        links = await Link.get_list(db, report.id)
        report.links = links

        return report

    @classmethod
    async def get_list(cls, db, user, limit='ALL', offset=0):
        result = await db.execute(
            "SELECT * FROM raports where user_id = '{}' LIMIT {} OFFSET {}".format(user.id, limit, offset)
        )
        result = result.fetchall()

        return [cls(*r) for r in result]

    @classmethod
    async def create(cls, db, payload):
        id = (await db.execute(
            "INSERT INTO raports (status, user_id, project_id) VALUES ('{}', '{}', '{}') RETURNING id".format(
                payload['status'], payload['user_id'], payload['project_id']
            )
        )).fetchone()[0]

        return cls(id=id, **payload)

    def serialize(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'status': self.status,
            'project_id': self.project_id,
            'links': [link.serialize() for link in self.links]
        }
