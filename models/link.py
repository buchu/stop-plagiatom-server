from models.base import BaseModel
from models.word import Word


class Link(BaseModel):
    id = None
    report_id = None
    cosine_similarity = None
    link_path = None
    jaccard_index = None
    words = []

    def __init__(self, id, report_id, cosine_similarity, link_path, jaccard_index, *args, **kwargs):
        self.id = id
        self.report_id = report_id
        self.cosine_similarity = cosine_similarity
        self.link_path = link_path
        self.jaccard_index = jaccard_index

    @classmethod
    async def get_list(cls, db, report_id):
        results = await db.execute(
            "SELECT * from suspicious WHERE raport_id = '{}'".format(report_id)
        )

        results = results.fetchall()
        links = [cls(*r) for r in results]
        for link in links:
            words = await Word.get_list(db, link.id)
            link.words = words

        return links

    @classmethod
    async def create(cls, db, payload, report_id):
        id = (await db.execute(
            "INSERT INTO suspicious (raport_id, cosine_similarity, link_path, jaccard_index) VALUES ('{}', '{}', '{}', '{}') RETURNING id".format(
                report_id, payload['cosine_similarity'], payload['link_path'], payload['jaccard_index']
            )
        )).fetchone()[0]

        return cls(id=id, **payload, report_id=report_id)

    def serialize(self):
        return {
            'id': self.id,
            'cosine_similarity': self.cosine_similarity,
            'link_path': self.link_path,
            'jaccard_index': self.jaccard_index,
            'words': [w.serialize() for w in getattr(self, 'words', [])]
        }
