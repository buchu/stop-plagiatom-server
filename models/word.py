from models.base import BaseModel


class Word(BaseModel):
    id = None
    link_id = None
    sequence = None

    def __init__(self, id, sequence, link_id):
        self.id = id
        self.link_id = link_id
        self.sequence = sequence

    @classmethod
    async def create(cls, db, link_id, sequence):
        id = (await db.execute(
            "insert into words (suspicious_id, sequence) values ({}, '{}') returning id;".format(link_id, sequence)
        )).fetchone()[0]

        return cls(id=id, link_id=link_id, sequence=sequence)

    @classmethod
    async def get_list(cls, db, link_id):
        results = (await db.execute(
            "select * from words where suspicious_id = {}".format(link_id)
        )).fetchall()

        return [cls(*r) for r in results]

    def serialize(self):
        return {
            'id': self.id,
            'link_id': self.link_id,
            'sequence': self.sequence
        }
