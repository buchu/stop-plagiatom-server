import hashlib
import random
import string

from tornado.options import options

from models.base import BaseModel
from sparkpost import SparkPost, SparkPostException


class User(BaseModel):
    id = None
    first_name = None
    last_name = None
    email = None
    password_hash = None
    is_admin = False

    def __init__(self, id, email, first_name, last_name, password_hash=None, is_admin=False):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password_hash = password_hash
        self.is_admin = is_admin

    @staticmethod
    def _send_email_with_password(password, user):
        sparky = SparkPost("a327dc7ffe14ecb79720ae867226b891ba77243d")
        sparky.transmissions.send(
            use_sandbox=True,
            recipients=[user.email],
            html='<html><body><p>H! here is your password: {}</p></body></html>'.format(password),
            from_email='testing@sparkpostbox.com',
            subject='Oh hey!')

    @staticmethod
    def _generate_password(n=8):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))

    @staticmethod
    def _hash_password(password):
        return hashlib.sha512((password + options.SALT).encode('utf-8')).hexdigest()

    @classmethod
    async def get_by_email(cls, db, email):
        data = await db.execute("SELECT * FROM users where email = '{}';".format(email))
        data = data.fetchone()

        return cls(*data)

    @classmethod
    async def store(cls, db, data):
        password = cls._generate_password()
        password_hash = cls._hash_password(password)
        id = (await db.execute(
            "INSERT INTO users (first_name, last_name, email, password_hash, is_admin) values ('{}', '{}', '{}', '{}', {}) RETURNING id;".format(
                data.get('first_name'), data.get('last_name'), data.get('email'), password_hash, data.get('is_admin', False)
            ))).fetchone()[0]

        user = cls(**data, id=id)
        try:
            cls._send_email_with_password(password, user)
        except SparkPostException as e:
            print(str(e))

        return user

    @classmethod
    async def log_in(cls, db, email, password):
        hashed_password = cls._hash_password(password)
        result = await db.execute(
            "SELECT first_name, last_name, email, is_admin FROM users where email = '{}' and password_hash = '{}';".format(email,
                                                                                                                 hashed_password)
        )
        result = result.fetchone()

        if result:
            return {
                'first_name': result[0],
                'last_name': result[1],
                'email': result[2],
                'is_admin': result[3]
            }
        else:
            return {}

    @classmethod
    async def get_list(cls, db, limit='ALL', offset=0):
        result = await db.execute(
            "SELECT * from users limit {} offset {};".format(limit, offset)
        )
        result = result.fetchall()

        return [cls(*params) for params in result]

    @classmethod
    async def get(cls, db, user_id):
        result = (await db.execute(
            "SELECT * from users WHERE id = {}".format(user_id)
        )).fetchone()

        return cls(*result)

    @classmethod
    async def delete(cls, db, user_id):
        cursor = (await db.execute(
            "DELETE FROM users WHERE id = '{}'".format(user_id)
        ))

        return cursor.rowcount == 1

    @classmethod
    async def update(cls, db, user_id, data):
        id = (await db.execute(
            "UPDATE users set first_name='{}', last_name='{}', email='{}' where id = {} returning id".format(
                data.get('first_name'), data.get('last_name'), data.get('email'), user_id
            ))).fetchone()[0]

        return cls(**data, id=id)

    def serialize(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'is_admin': self.is_admin
        }
