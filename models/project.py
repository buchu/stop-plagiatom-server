import datetime

from models.base import BaseModel


class Project(BaseModel):
    id = None
    author = None
    title = None
    content = None
    user_id = None
    submitted = None
    is_deleted = None
    report_id = None
    report_status = None

    def __init__(self, id, author, title, content, user_id, submitted, is_deleted, report_id=None, report_status=None):
        self.id = id
        self.author = author
        self.title = title
        self.content = content
        self.submitted = submitted
        self.user_id = user_id
        self.is_deleted = is_deleted
        self.report_id = report_id
        self.report_status = report_status

    @classmethod
    async def get(cls, db, user_id, project_id):
        result = (await db.execute(
            "SELECT projects.*, raports.id as report_id, raports.status as report_status FROM "
            "projects LEFT JOIN raports ON projects.id = raports.project_id WHERE projects.user_id = '{}' AND projects.id = '{}' AND projects.is_deleted != TRUE;".format(
                user_id, project_id)
        )).fetchone()

        return cls(*result) if result else None

    @classmethod
    async def get_list(cls, db, user, offset=0, limit='ALL'):
        result = await db.execute(
            "SELECT projects.*, raports.id as report_id, raports.status as report_status FROM "
            "projects LEFT JOIN raports ON projects.id = raports.project_id where projects.user_id = '{}' and projects.is_deleted != TRUE limit {} offset {}".format(
                user.id, limit, offset)
        )
        result = result.fetchall()

        return [cls(*p) for p in result]

    @classmethod
    async def create(cls, db, payload, user_id):
        now = datetime.datetime.now()
        id = (await db.execute(
            "INSERT INTO projects (author, title, content, user_id, submitted, is_deleted) "
            "VALUES ('{}', '{}', '{}', '{}', '{}', false) RETURNING id".format(
                payload['author'], payload['title'], payload['content'], user_id, now
            )
        )).fetchone()[0]

        return cls(**payload, user_id=user_id, submitted=now, id=id, is_deleted=False)

    @classmethod
    async def delete(cls, db, project_id, user_id):
        cursor = (await db.execute(
            "UPDATE projects SET is_deleted = TRUE WHERE id = '{}' AND user_id = '{}'".format(project_id, user_id)
        ))

        return cursor.rowcount == 1

    @classmethod
    async def get_user_id(cls, db, id):
        user_id = (await db.execute(
            'select user_id from projects where id = {}'.format(id)
        )).fetchone()[0]

        return user_id

    def serialize(self, with_content=False):
        result = {
            'id': self.id,
            'author': self.author,
            'title': self.title,
            'user_id': self.user_id,
            'submitted': self.submitted.isoformat(),
            'report_id': self.report_id,
            'report_status': self.report_status
        }

        if with_content:
            result['content'] = self.content

        return result
