import datetime
from json import JSONDecodeError

import jwt
from models.link import Link
from models.project import Project
from models.report import Report
from models.word import Word

from tornado.web import RequestHandler
from tornado.options import options
from tornado.escape import json_decode

from models import User
from utils.auth import jwtauth


class BaseHandler(RequestHandler):
    @property
    def db(self):
        return self.application.db

    async def get_current_user(self):
        auth = self.request.headers.get('Authorization')
        if auth and len(auth.split()) == 2:
            auth = auth.split()

            user_payload = jwt.decode(
                auth[1],
                options.SECRET
            )

            user = await User.get_by_email(self.db, user_payload['email'])
            return user


class MainHandler(BaseHandler):
    def get(self):
        self.write({
            'status': 'OK',
            'isotime': datetime.datetime.now().isoformat()
        })


class UsersHandler(BaseHandler):
    async def get(self):
        offset = self.get_argument('offset', default=0)
        limit = self.get_argument('limit', default=10)

        users = await User.get_list(self.db, limit=limit, offset=offset)

        self.write({
            'results': [user.serialize() for user in users],
            'limit': limit,
            'offset': offset
        })

    async def post(self):
        try:
            data = json_decode(self.request.body)
            assert all(k in data.keys() for k in ['email', 'first_name', 'last_name'])
        except AssertionError:
            self.set_status(status_code=400)
            self.write({'error': 'Fields are required: email, first_name, last_name'})
        except JSONDecodeError as e:
            self.set_status(status_code=400)
            self.write({'error': str(e)})
        else:
            user = await User.store(self.db, data)

            self.set_status(201)
            self.write(user.serialize())


class UserHandler(BaseHandler):
    async def get(self, user_id):
        user = await User.get(self.db, user_id)

        self.write(user.serialize())

    async def delete(self, user_id):
        deleted = await User.delete(self.db, user_id)

        if deleted:
            self.set_status(204)
        else:
            self.set_status(404)

    async def put(self, user_id):
        data = json_decode(self.request.body)

        user = await User.update(self.db, user_id, data)

        self.write(user.serialize())


@jwtauth
class ReportsHandler(BaseHandler):
    no_auth = ['post']

    async def get(self):
        user = await self.get_current_user()

        offset = self.get_argument('offset', default=0)
        limit = self.get_argument('limit', default=10)

        reports = await Report.get_list(self.db, user=user, limit=limit, offset=offset)

        self.write({
            'results': [report.serialize() for report in reports],
            'offset': offset,
            'limit': limit
        })

    async def post(self):
        try:
            data = json_decode(self.request.body)
        except JSONDecodeError as e:
            self.set_status(status_code=400)
            self.write({'error': str(e)})
        else:
            status = 'OK' if len(data.get('links', [])) == 0 else 'WARN'
            user_id = await Project.get_user_id(self.db, data['project_id'])
            report = await Report.create(
                self.db,
                {'status': status, 'user_id': user_id, 'project_id': data['project_id']})
            links = []
            for link_data in data.get('links', []):
                link_words = []
                sequences = link_data.pop('sequences', [])
                link = await Link.create(self.db, link_data, report.id)
                for word in sequences:
                    word = await Word.create(self.db, sequence=word, link_id=link.id)
                    link_words.append(word)
                link.words = link_words
                links.append(link)
            report.links = links

            self.set_status(201)
            self.write(report.serialize())


@jwtauth
class ReportHandler(BaseHandler):
    async def get(self, report_id):
        user = await self.get_current_user()

        report = await Report.get(self.db, user.id, report_id)

        if report:
            self.write(report.serialize())
        else:
            self.set_status(404)


@jwtauth
class ProjectsHandler(BaseHandler):
    async def get(self):
        user = await self.get_current_user()

        offset = self.get_argument('offset', default=0)
        limit = self.get_argument('limit', default=10)

        projects = await Project.get_list(self.db, user, offset=offset, limit=limit)

        self.write({
            'offset': offset,
            'limit': limit,
            'results': [p.serialize() for p in projects]
        })

    async def post(self):
        user = await self.get_current_user()
        try:
            data = json_decode(self.request.body)
            project = await Project.create(self.db, data, user.id)
        except KeyError as e:
            self.set_status(400)
            self.write({'error': "'{}' is required".format(str(e))})
        else:
            self.set_status(201)
            self.write(project.serialize(with_content=True))


@jwtauth
class ProjectHandler(BaseHandler):
    async def get(self, project_id):
        user = await self.get_current_user()

        project = await Project.get(self.db, user.id, project_id)

        if project:
            self.write(project.serialize(with_content=True))
        else:
            self.set_status(404)

    async def delete(self, project_id):
        user = await self.get_current_user()
        deleted = await Project.delete(self.db, project_id, user.id)
        if not deleted:
            self.set_status(404)
        else:
            self.set_status(204)


class AuthHandler(BaseHandler):
    async def post(self):
        try:
            data = json_decode(self.request.body)
            email, password = data['email'], data['password']
        except JSONDecodeError:
            self.set_status(400)
            self.write({'error': 'Email and passwords are required.'})
        else:

            payload = await User.log_in(self.db, email, password)

            if not payload:
                self.set_status(400)
                self.write({
                    'error': 'Wrong credentials.'
                })
            else:
                token = jwt.encode(
                    payload=payload,
                    key=options.SECRET
                )

                self.write({
                    'token': token.decode('utf-8')
                })
