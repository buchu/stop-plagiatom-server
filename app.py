import tornado.web
import momoko

from tornado.ioloop import IOLoop

from urls import handlers

from tornado.options import options, parse_config_file
from tornado.httpserver import HTTPServer


class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            cookie_secret=options.SECRET,
            xsrf_cookies=False,
            debug=True
        )

        super().__init__(handlers, **settings)
        self.ioloop = IOLoop.instance()

        self.db = momoko.Pool(
            dsn='dbname={} user={} password={} '
                'host={} port={}'.format(options.db_name,
                                         options.db_user,
                                         options.db_pass,
                                         options.db_host,
                                         options.db_port),
            size=1,
            ioloop=self.ioloop
        )


def main():
    parse_config_file('./config.py')
    application = Application()

    future = application.db.connect()
    application.ioloop.add_future(future, lambda f: application.ioloop.stop())
    application.ioloop.start()

    http_server = HTTPServer(application)
    http_server.listen(options.PORT)

    IOLoop.current().start()


if __name__ == "__main__":
    main()
