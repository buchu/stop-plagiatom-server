from tests import APITestCase


class AuthHandlerTests(APITestCase):
    def test_should_get_400_with_empty_body(self):
        response = self.post('/login')

        assert response.status_code == 400

    def test_should_return_error_for_wrong_credentials(self):
        response = self.post('/login', payload={'email': 'test@test.pl', 'password': 'test123'})

        assert response.status_code == 400
        assert 'error' in response.json()
        assert 'Wrong credentials.' == response.json()['error']

    def test_should_return_access_token_for_logged_in_user(self):
        response = self.post('/login', payload={'email': 'test@gmail.com', 'password': 'test'})

        assert response.status_code == 200
        assert 'token' in response.json()
