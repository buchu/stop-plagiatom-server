import os
import sys

path = os.path.abspath(os.path.join(os.getcwd(), '..'))
if path not in sys.path:
    sys.path.append(path)

import inspect
import traceback

from tests.main_tests import MainHendlerTests
from tests.auth_tests import AuthHandlerTests
from tests.raports_tests import RaportsHandlerTests
from tests.projects_tests import ProjectsHandlerTests


def main():
    failed = 0
    succeded = 0
    suits = [
        MainHendlerTests(),
        AuthHandlerTests(),
        RaportsHandlerTests(),
        ProjectsHandlerTests()
    ]

    for suit in suits:
        suit_name = suit.__class__.__name__
        print('-----------------')
        print('Running suit for: {}'.format(suit_name))
        for test in inspect.getmembers(suit, predicate=inspect.ismethod):
            name, fn = test
            if name.startswith('test_'):
                try:
                    print('- Running: {}'.format(name))
                    fn()
                except AssertionError as e:
                    print('{}.{} failed with:'.format(suit_name, name))
                    traceback.print_exc(file=sys.stdout)
                    failed += 1
                else:
                    print('.')
                    succeded += 1

    print('Runned succeded: {} / failed: {} / all: {}'.format(succeded, failed, succeded + failed))

if __name__ == '__main__':

    main()