from tests import APITestCase


class RaportsHandlerTests(APITestCase):

    def test_if_urls_exists(self):
        endpoints = [
            {'url': '/reports', 'method': 'get', 'args': {}},
            {'url': '/reports/{id}', 'method': 'get', 'args': {'id': 1}}
        ]

        for endpoint in endpoints:
            response = self.get(endpoint['url'].format(**endpoint['args']))
            assert response.status_code in (200, 201, 204)

    # REPORTS
    def test_reports_should_return_list(self):
        response = self.get('/reports')
        body = response.json()

        assert 'results' in body
        assert isinstance(body['results'], list)

    def test_reports_should_not_be_open_for_logged_in_user(self):
        response = self.get('/reports', auth=False)

        assert response.status_code == 403

    def test_reports_should_return_reports_of_logged_user(self):
        response = self.get('/reports')
        body = response.json()

        assert len(body['results']) > 0
        assert body['results'][0]['user_id'] == 1

    # REPORT
    def test_report_should_return_object(self):
        response = self.get('/reports/1')
        body = response.json()

        assert isinstance(body, dict)

    def test_report_should_return_instance_of_report(self):
        response = self.get('/reports/1')
        body = response.json()

        assert 'id' in body
        assert 'user_id' in body
        assert 'links' in body

    def test_should_return_404_on_random_id(self):
        response = self.get('/reports/1111')

        assert response.status_code == 404

    def test_report_should_be_auth_only(self):
        response = self.get('/reports/1', auth=False)

        assert response.status_code == 403
