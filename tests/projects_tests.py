from tests import APITestCase


class ProjectsHandlerTests(APITestCase):
    def test_projects_should_return_list(self):
        response = self.get('/projects')
        body = response.json()

        assert 'results' in body
        assert isinstance(body['results'], list)
    
    def test_projects_should_not_be_open_for_logged_in_user(self):
        response = self.get('/projects', auth=False)
    
        assert response.status_code == 403
    
    def test_projects_should_return_projects_of_logged_user(self):
        response = self.get('/projects')
        body = response.json()
    
        assert len(body['results']) > 0
        assert body['results'][0]['user_id'] == 1

    def test_projects_should_return_projects_without_content(self):
        response = self.get('/projects')
        body = response.json()

        assert len(body['results']) > 0
        assert 'content' not in body['results'][0]
    
    # PROJECT
    def test_project_should_return_object(self):
        response = self.get('/projects/1')
        body = response.json()

        assert isinstance(body, dict)

    def test_project_should_return_instance_of_project(self):
        response = self.get('/projects/1')
        body = response.json()

        assert 'id' in body
        assert 'user_id' in body
        assert 'title' in body
        assert 'author' in body
        assert 'content' in body

    def test_should_return_404_on_random_id(self):
        response = self.get('/projects/1111')

        assert response.status_code == 404

    def test_project_should_be_auth_only(self):
        response = self.get('/projects/1', auth=False)

        assert response.status_code == 403

    def test_project_post_should_require_all_data(self):
        response = self.post('/projects', {})

        assert response.status_code == 400

    def test_project_post_should_create_one(self):
        data = {
            'author': 'test',
            'title': 'test2',
            'content': 'testtesttest'
        }
        response = self.post('/projects', data)

        assert response.status_code == 201

    def test_project_delete_should_throw_404(self):
        response = self.delete('/projects/99999')

        assert response.status_code == 404

    def test_project_delete_should_return_204(self):
        response = self.delete('/projects/2')

        assert response.status_code == 204
