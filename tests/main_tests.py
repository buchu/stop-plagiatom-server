import requests

from tests import get_url


class MainHendlerTests:
    def test_if_url_exists(self):
        response = requests.get(get_url('/'))

        assert response.status_code == 200


