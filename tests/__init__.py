import requests

from urllib.parse import urljoin


def get_url(url):
    return urljoin('http://localhost:8888', url)


class APITestCase:
    headers = {
        'Authorization': 'Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYXN0X25hbWUiOiJ0ZXN0IiwiZmlyc3RfbmFtZSI6InRlc3QiLCJlbWFpbCI6InRlc3RAZ21haWwuY29tIn0.ruKZOVfZhn0gl7E0JndGWRA0y93bhi9hwVOjsy1rlT0'
    }

    def get(self, url, auth=True):
        if auth:
            return requests.get(get_url(url), headers=self.headers)
        else:
            return requests.get(get_url(url))

    def post(self, url, payload=None, auth=True):
        if auth:
            return requests.post(get_url(url), json=payload, headers=self.headers)
        else:
            return requests.post(get_url(url), json=payload)

    def delete(self, url, auth=True):
        if auth:
            return requests.delete(get_url(url), headers=self.headers)
        else:
            return requests.delete(get_url(url))
