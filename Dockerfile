FROM python:3

MAINTAINER Pawel Buchowski <pawel.buchowski@gmail.com>

RUN apt-get update -y && apt-get install git python-setuptools python-dev build-essential python3 python-pip -y


RUN mkdir -p /tmp/stop-plagiatom-server
WORKDIR /tmp/stop-plagiatom-server

COPY requirements.txt /tmp/stop-plagiatom-server
RUN pip install -r requirements.txt

COPY . /tmp/stop-plagiatom-server
