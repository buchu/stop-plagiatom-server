import handlers as h

handlers = [
    (r"/", h.MainHandler),
    (r"/login", h.AuthHandler),
    (r"^/reports$", h.ReportsHandler),
    (r"^/reports/([^/]+)$", h.ReportHandler),
    (r"/projects", h.ProjectsHandler),
    (r"^/projects/([^/]+)$", h.ProjectHandler),
    (r"/users", h.UsersHandler),
    (r"^/users/([^/]+)$", h.UserHandler),
]
