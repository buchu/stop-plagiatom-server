import jwt
from tornado.options import options


class NotAuthorizedException(Exception):
    pass


def jwtauth(handler_class):
    def wrap_execute(handler_execute):
        def require_auth(handler, kwargs):
            if handler.request.method.lower() in getattr(handler, 'no_auth', []):
                return

            auth = handler.request.headers.get('Authorization')

            try:
                parts = auth.split()

                jwt.decode(
                    parts[1],
                    options.SECRET
                )

                if len(parts) != 2 or parts[0].lower() != 'token':
                    raise NotAuthorizedException()

            except (NotAuthorizedException, Exception):
                handler._transforms = []
                handler.set_status(403)
                handler.write('Unauthorized')
                handler.finish()

            return True

        def _execute(self, transforms, *args, **kwargs):
            require_auth(self, kwargs)

            return handler_execute(self, transforms, *args, **kwargs)

        return _execute

    handler_class._execute = wrap_execute(handler_class._execute)
    return handler_class
