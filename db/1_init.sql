CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    email varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    password_hash varchar(511) NOT NULL
);

CREATE TABLE IF NOT EXISTS projects (
    id SERIAL PRIMARY KEY,
    author varchar(255) NOT NULL,
    title varchar(255) NOT NULL,
    content varchar(255) NOT NULL,
    user_id integer REFERENCES users
);

CREATE TABLE IF NOT EXISTS raports (
    id SERIAL PRIMARY KEY,
    user_id integer REFERENCES users
);

CREATE TABLE IF NOT EXISTS suspicious (
    id SERIAL PRIMARY KEY,
    start integer NOT NULL,
    "end" integer NOT NULL,
    url varchar(255) NOT NULL,
    raport_id integer REFERENCES raports
);

CREATE TABLE IF NOT EXISTS words (
    id SERIAL PRIMARY KEY,
    sequence text,
    suspicious_id integer REFERENCES suspicious
);

ALTER TABLE projects ADD COLUMN submitted date;
ALTER TABLE projects ALTER COLUMN content TYPE text;
ALTER TABLE projects ALTER COLUMN submitted TYPE timestamp;
ALTER TABLE raports ADD COLUMN status varchar(16);
ALTER TABLE raports ADD COLUMN project_id integer REFERENCES projects;
ALTER TABLE suspicious ADD COLUMN cosine_similarity varchar(32);
ALTER TABLE suspicious ADD COLUMN link_path varchar(255);
ALTER TABLE suspicious ADD COLUMN jaccard_index varchar(32);
alter table suspicious drop column start;
alter table suspicious drop column "end";
alter table suspicious drop column url;
alter table projects add column is_deleted boolean;
alter table users add column is_admin boolean;