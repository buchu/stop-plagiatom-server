DELETE FROM words;
DELETE FROM suspicious;
DELETE FROM raports;
DELETE FROM projects;
DELETE FROM users;

INSERT INTO users (id, first_name, last_name, email, password_hash) values (1, 'test', 'test', 'test@gmail.com', 'ddfbcd9738e933501c8c547b06fc6593fa7da5b8ca52030f3bbe0d3c8258ed76b8b5728c661197ddb9bb533e42eda5d86a17a84c653a4f38a0af9ed1410520c0');
INSERT INTO projects (id, author, title, content, user_id, submitted) VALUES (1, 'author', 'title', 'content', 1, NOW());
INSERT INTO projects (id, author, title, content, user_id, submitted) VALUES (2, 'author', 'title', 'content', 1, NOW());
INSERT INTO raports (id, user_id, status, project_id) VALUES (1, 1, 'WARNING', 1);
INSERT INTO suspicious (id, raport_id, cosine_similarity, link_path, jaccard_index) VALUES (1, 1, 0, 'http://google.pl', 0.013);
INSERT INTO words (sequence, suspicious_id) values ('da da ads', 1);
